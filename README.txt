### Protocole d'utilisation ###

Afin de lancer le code source, vous devez lancer le fichier "main.py" avec une version de python équivalente ou ultérieur à la version 3.10.
Pour lancer l'exécutable, vous devez simplement lancer le fichier "Sudokool.exe" sur windows. Si une fenêtre d'avertissement apparaît, cliquez sur "plus d'informations" puis "exécuter quand même".